<?php

namespace ATM\CommentBundle\Services;

use ATM\CommentBundle\Entity\Tree;

class CommentManager{

    private $container;
    private $configuration;
    private $em;
    private $paginator;

    public function __construct($container){
        $this->container = $container;
        $this->configuration = $this->container->getParameter('atm_comment_config');
        $this->em = $this->container->get('doctrine.orm.default_entity_manager');
        $this->paginator = $this->container->get('knp_paginator');
    }


    public function findThreadById($threadId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial t.{id, isCommentable,numComments,lastCommentAt}')
            ->from($this->configuration['class']['model']['thread'],'t')
            ->where(
                $qb->expr()->eq('t.id',$qb->expr()->literal($threadId))
            )
        ;

        $thread = $qb->getQuery()->getArrayResult();

        if(!empty($thread)){
            return $thread[0];
        }else{
            return null;
        }
    }

    public function getThreadComments($threadId,$max_results = 15,$page = 1){
        $qbIds = $this->em->createQueryBuilder();

        $qbIds
            ->select('c.id')
            ->from($this->configuration['class']['model']['comment'],'c')
            ->join('c.thread','t','WITH',$qbIds->expr()->eq(
                't.id',$qbIds->expr()->literal($threadId)
            ))
            ->where($qbIds->expr()->isNull('c.ancestors'))
            ->orderBy('c.id','DESC');

        $resultIds = array_map(function($c){
            return $c['id'];
        },$qbIds->getQuery()->getArrayResult());

        if(count($resultIds) > 0){
            $pagination = $this->paginator->paginate(
                $resultIds,
                $page,
                $max_results
            );
            $ids = $pagination->getItems();

            if(count($ids) > 0){
                $qb = $this->em->createQueryBuilder();
                $qb
                    ->select('partial c.{id,body,depth,createdAt,ancestors}')
                    ->addSelect('partial t.{id, isCommentable,numComments,lastCommentAt}')
                    ->addSelect('a')
                    ->from($this->configuration['class']['model']['comment'],'c')
                    ->join('c.thread','t')
                    ->join('c.author','a')
                    ->where(
                        $qb->expr()->orX(
                            $qb->expr()->in('c.id',$ids),
                            $qb->expr()->in('c.ancestors',$ids)
                        )
                    )
                    ->orderBy('c.id','ASC');

                $comments = $qb->getQuery()->getArrayResult();

                return $this->organiseComments($comments);
            }else{
                return array();
            }
        }else{
            return array();
        }
    }

    public function findAncestorsByCommentId($commentId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('partial c.{id,ancestors}')
            ->from($this->configuration['class']['model']['comment'],'c')
            ->where(
                $qb->expr()->eq('c.id',$commentId)
            );

        $comment = $qb->getQuery()->getArrayResult();
        return $comment[0]['ancestors'];
    }

    public function getTotalComments($threadId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('COUNT(c.id)')
            ->from($this->configuration['class']['model']['comment'],'c')
            ->join('c.thread','t','WITH',$qb->expr()->eq(
                't.id',$qb->expr()->literal($threadId)
            ));

        return $qb->getQuery()->getArrayResult();
    }

    public function getChildCommentsFromComment($commentId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('c')
            ->from($this->configuration['class']['model']['comment'],'c')
            ->where(
                $qb->expr()->like('c.ancestors',$qb->expr()->literal('%'.$commentId.'%'))
            );

        return $qb->getQuery()->getResult();
    }

    protected function organiseComments($comments)
    {
        $tree = new Tree();

        foreach ($comments as $comment) {
            $path = $tree;

            if(!empty($comment['ancestors'])){
                $ancestors = explode('/',$comment['ancestors']);
                foreach ($ancestors as $ancestor) {
                    $path = $path->traverse($ancestor);
                }
            }
            $path->add($comment);
        }

        $tree = $tree->toArray();
        $tree = $this->sort($tree);

        return $tree;
    }

    private function sort($tree)
    {
        foreach ($tree as $branch) {
            if (count($branch['children'])) {
                $branch['children'] = $this->sort($branch['children']);
            }
        }

        usort($tree, array($this, 'doSort'));

        return $tree;
    }

    private function doSort($comment1 , $comment2){
        $sorting = $this->configuration['sorting']['default'];

        if(strtoupper($sorting) == 'ASC'){
            return $this->compare($comment1 , $comment2);
        }else{
            return $this->compare($comment2 , $comment1);
        }
    }

    private function compare($comment1,$comment2){

        $createdAt1 = $comment1['comment']['createdAt']->getTimestamp();
        $createdAt2 = $comment2['comment']['createdAt']->getTimestamp();

        if ($createdAt1 == $createdAt2){
            return 0;
        }

        return $createdAt1 < $createdAt2 ? -1 : 1;
    }
}