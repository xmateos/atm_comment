<?php

namespace ATM\CommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class Thread{

    protected $id;

    /**
     * @ORM\Column(name="is_commentable", type="boolean", nullable=false)
     */
    protected $isCommentable = true;

    /**
     * @ORM\Column(name="num_comments", type="integer", nullable=false)
     */
    protected $numComments = 0;

    /**
     * @ORM\Column(name="last_created_at", type="datetime", nullable=true)
     */
    protected $lastCommentAt = null;


    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function isCommentable()
    {
        return $this->isCommentable;
    }

    public function setCommentable($isCommentable)
    {
        $this->isCommentable = (bool) $isCommentable;
    }

    public function getNumComments()
    {
        return $this->numComments;
    }

    public function setNumComments($numComments)
    {
        $this->numComments = intval($numComments);
    }

    public function incrementNumComments($by = 1)
    {
        return $this->numComments += intval($by);
    }

    public function getLastCommentAt()
    {
        return $this->lastCommentAt;
    }

    public function setLastCommentAt($lastCommentAt)
    {
        $this->lastCommentAt = $lastCommentAt;
    }

    public function __toString()
    {
        return 'Comment thread #'.$this->getId();
    }
}
