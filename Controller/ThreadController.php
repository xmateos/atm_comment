<?php

namespace ATM\CommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ATM\CommentBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Response;
use ATM\CommentBundle\Event\Comment as CommentEvent;
use ATM\BadgeBundle\Event\RuleEvent;
use \DateTime;

class ThreadController extends Controller
{

    public function getThreadAction($threadId,$max_results,$page){

        $atmConfig = $this->getParameter('atm_comment_config');
        $depth = $atmConfig['depth'];
        $commentManager = $this->get('atm_comment_manager');

        $thread = $commentManager->findThreadById($threadId);
        $threadComments = array();
        if($thread){
            $threadComments = $commentManager->getThreadComments($threadId,$max_results,$page);
        }

        foreach($threadComments as $key => $comment){
            if(count($comment['children']) > 0){
                usort($comment['children'],array($this,'doSort'));
                $threadComments[$key] = $comment;
            }
        }

        return $this->render('ATMCommentBundle:Thread:overview.html.twig',array(
            'thread' => $thread,
            'depth' => $depth,
            'comments' => $threadComments,
            'threadId' => $threadId
        ));
    }

    public function loadMoreAction($threadId,$max_results,$page){
        $atmConfig = $this->getParameter('atm_comment_config');
        $depth = $atmConfig['depth'];
        $commentManager = $this->get('atm_comment_manager');

        $thread = $commentManager->findThreadById($threadId);
        $threadComments = array();
        if($thread){
            $threadComments = $commentManager->getThreadComments($threadId,$max_results,$page);
        }

        foreach($threadComments as $key => $comment){
            if(count($comment['children']) > 0){
                usort($comment['children'],array($this,'doSort'));
                $threadComments[$key] = $comment;
            }
        }

        return $this->render('ATMCommentBundle:Thread:comments.html.twig',array(
            'depth' => $depth,
            'comments' => $threadComments
        ));
    }

    private function doSort($comment1,$comment2){
        return $this->sortComments($comment2,$comment1);
    }

    private function sortComments($comment1,$comment2){
        $atmConfig = $this->getParameter('atm_comment_config');
        $createdAt1 = $comment1['comment']['createdAt']->getTimestamp();
        $createdAt2 = $comment2['comment']['createdAt']->getTimestamp();

        if ($createdAt1 == $createdAt2){
            return 0;
        }

        if($atmConfig['sorting']['subcomments'] == 'DESC'){
            return $createdAt1 < $createdAt2 ? -1 : 1;
        }else{
            return $createdAt1 < $createdAt2 ? 1 : -1;
        }
    }

    public function newCommentFormAction($threadId = null){
        $atmConfig = $this->getParameter('atm_comment_config');

        $comment = new $atmConfig['class']['model']['comment']();
        $form = $this->createForm(CommentType::class, $comment);

        $request = $this->get('request_stack')->getCurrentRequest();
        $parentCommentId = $request->get('pci');

        return $this->render('ATMCommentBundle:Thread:new.html.twig',array(
            'form' => $form->createView(),
            'threadId' => $threadId,
            'parentCommentId' => $parentCommentId
        ));
    }

    public function newCommentSubmitAction(){
        $atmConfig = $this->getParameter('atm_comment_config');
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $request = $this->get('request_stack')->getCurrentRequest();
        $body = $request->get('b');
        $threadId = $request->get('ti');
        $parentCommentId = $request->get('pci');

        $thread = $em->getRepository($atmConfig['class']['model']['thread'])->findOneBy(array('id'=>$threadId));

        if(is_null($thread)){
            $thread = new $atmConfig['class']['model']['thread']();
            $thread->setId($threadId);
        }


        $comment = new $atmConfig['class']['model']['comment']();
        $comment->setBody(strip_tags(html_entity_decode($body),'<span>'));
        $comment->setAuthor($user);
        $comment->setThread($thread);
        $thread->incrementNumComments();
        $thread->setLastCommentAt(new DateTime());

        if($parentCommentId){
            $commentManager = $this->get('atm_comment_manager');
            //add parent comment ancestors to the new comment ancestor field
            $parentCommentAncestors = $commentManager->findAncestorsByCommentId($parentCommentId);
            if($parentCommentAncestors){
                $ancestors = explode('/',$parentCommentAncestors .'/'.$parentCommentId);
            }else{
                $ancestors = array($parentCommentId);
            }
            $comment->setAncestors($ancestors);
        }

        $em->persist($thread);
        $em->persist($comment);
        $em->flush();

        // Dispatch event
        $event = new CommentEvent($comment);
        $this->get('event_dispatcher')->dispatch(CommentEvent::NAME, $event);

        if(class_exists('RuleEvent')){
            //Dispatch event for badges
            $ruleEvent = new RuleEvent($user,'comment');
            $this->get('event_dispatcher')->dispatch(RuleEvent::NAME,$ruleEvent);
        }




        return $this->render('ATMCommentBundle:Thread:comment.html.twig',array(
            'comment' => $comment,
            'depth' => $atmConfig['depth']
        ));
    }

    public function deleteCommentAction($commentId){
        $em = $this->getDoctrine()->getManager();
        $atmConfig = $this->getParameter('atm_comment_config');

        $comment = $em->getRepository($atmConfig['class']['model']['comment'])->findOneById($commentId);
        $thread = $comment->getThread();

        $this->deleteChildComments($comment,$em);
        $em->remove($comment);

        $numCommentsThread = $thread->getNumComments() - 1;
        if($numCommentsThread < 0){
            $numCommentsThread = 0;
        }
        $thread->setNumComments($numCommentsThread);
        $em->persist($thread);

        $em->flush();

        return new Response(json_encode(array(
            'numComments' => $numCommentsThread
        )));
    }

    private function deleteChildComments($comment,$em){
        $commentManager = $this->get('atm_comment_manager');
        $childComments = $commentManager->getChildCommentsFromComment($comment->getId());

        foreach($childComments as $childComment){
            $em->remove($childComment);
        }
    }
}
