<?php

namespace ATM\CommentBundle\Extension;

class CommentExtension extends \Twig_Extension
{
    private $commentManager;


    public function __construct($commentManager){
        $this->commentManager = $commentManager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getATMTotalComments', array($this, 'getATMTotalComments')),
        );
    }

    public function getATMTotalComments($threadId){
        $totalComments = $this->commentManager->getTotalComments($threadId);

        if(!empty($totalComments)){
            return $totalComments[0][1];
        }else{
            return 0;
        }
    }


    public function getName()
    {
        return 'comment';
    }
}
