<?php

namespace ATM\CommentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('body',TextareaType::class,array(
                'attr' => array(
                    'class' => 'atm_comment_bundle_body'
                )
            ))
        ;
    }

    public function getBlockPrefix()
    {
        return 'atmcomment_bundle_comment';
    }
}
